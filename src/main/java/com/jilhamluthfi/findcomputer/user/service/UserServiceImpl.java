package com.jilhamluthfi.findcomputer.user.service;

import com.jilhamluthfi.findcomputer.user.model.UserC;
import com.jilhamluthfi.findcomputer.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public List<UserC> getListOfUserLikeUsername(String username) {
        return userRepository.findAllUserLikeUsername(username);
    }

    @Override
    public UserC findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserC saveUser(UserC userC) {
        UserC user = new UserC();
        user.setFirstName(userC.getFirstName());
        user.setLastName(userC.getLastName());
        user.setUsername(userC.getUsername());
        user.setEmail(userC.getEmail());
        user.setPassword(passwordEncoder.encode(userC.getPassword()));
        user.setItemList(new ArrayList<>());
        return userRepository.save(user);
    }

    @Override
    public UserC updateUser(UserC oldUser, UserC updatedUser) {
        oldUser.setFirstName(updatedUser.getFirstName());
        oldUser.setLastName(updatedUser.getLastName());
        oldUser.setEmail(updatedUser.getEmail());
        return userRepository.save(oldUser);
    }

    @Override
    public boolean userIsExisted(String username) {
        UserC user = userRepository.findByUsername(username);
        if (user != null) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserC user = findByUsername(username);
        try {
            return new org.springframework.security.core.userdetails.User(
                    user.getUsername(), user.getPassword(), getAuthorities("ROLE_USER")
            );
        }
        catch (NullPointerException e) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
    }

    public Collection<? extends GrantedAuthority> getAuthorities(String role) {
        return Arrays.asList(new SimpleGrantedAuthority(role));
    }
}
