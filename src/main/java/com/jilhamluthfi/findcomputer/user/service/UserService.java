package com.jilhamluthfi.findcomputer.user.service;

import com.jilhamluthfi.findcomputer.user.model.UserC;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService {

    List<UserC> getListOfUserLikeUsername(String username);
    UserC findByUsername(String username);
    UserC saveUser(UserC userC);
    UserC updateUser(UserC oldUser, UserC updatedUser);
    boolean userIsExisted(String email);

}
