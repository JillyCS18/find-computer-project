package com.jilhamluthfi.findcomputer.user.controller;

import com.jilhamluthfi.findcomputer.user.model.UserC;
import com.jilhamluthfi.findcomputer.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/")
    public String root() {
        return "index";
    }

    @GetMapping(path = "/login")
    public String login() {
        return "login";
    }

    @GetMapping(path = "/home")
    public String home(HttpServletRequest request, Model model) {
        String username = (String) request.getSession().getAttribute("username");
        UserC user = userService.findByUsername(username);
        model.addAttribute("user", user);
        return "homepage";
    }

    @GetMapping(path = "/home/profile")
    public String profile(HttpServletRequest request, Model model) {
        String username = (String) request.getSession().getAttribute("username");
        UserC user = userService.findByUsername(username);
        model.addAttribute("user", user);
        model.addAttribute("updatedUser", new UserC());
        return "profile";
    }

    @PostMapping(path = "/home/profile")
    public String profile(@ModelAttribute("updatedUser") UserC updatedUser, HttpServletRequest request) {
        String username = (String) request.getSession().getAttribute("username");
        UserC user = userService.findByUsername(username);
        userService.updateUser(user, updatedUser);
        return "redirect:/home";
    }

    @GetMapping(path = "/success-login")
    public String successLogin(HttpServletRequest request, Authentication auth) {
        String username = auth.getName();
        request.getSession().setAttribute("username", username);
        return "redirect:/home";
    }
}
