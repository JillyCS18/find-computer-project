package com.jilhamluthfi.findcomputer.user.repository;

import com.jilhamluthfi.findcomputer.user.model.UserC;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserC, Long> {
    UserC findByUsername(String username);

    @Query(value = "select * from pengguna where username like ?1%",
            nativeQuery = true)
    List<UserC> findAllUserLikeUsername(String username);
}
