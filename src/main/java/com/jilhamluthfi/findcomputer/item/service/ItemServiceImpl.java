package com.jilhamluthfi.findcomputer.item.service;

import com.jilhamluthfi.findcomputer.item.model.Item;
import com.jilhamluthfi.findcomputer.item.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Override
    public List<Item> getListOfItemLikeName(String keyword, int ownerId) {
        return itemRepository.findAllItemLikeName(keyword, ownerId);
    }

    @Override
    public List<Item> getListOfItemByCategory(String category, int ownerId) {
        return itemRepository.findAllItemByCategoryAndOwner(category, ownerId);
    }

    @Override
    public Item save(Item item) {
        return itemRepository.save(item);
    }

    @Override
    public Item update(Item item) {
        return itemRepository.save(item);
    }

    @Override
    public Item getItemById(int id) {
        return itemRepository.findById(id);
    }

    @Override
    public void deleteItem(int id) {
        itemRepository.deleteById(id);
    }
}
